import React, { useEffect, useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  Row,
  Col,
  ListGroup,
  Image,
  Form,
  Button,
  Card,
  Alert,
} from 'react-bootstrap';
import { removeFromCart } from '../actions/cartActions';
import {
  getCartItemCount,
  getTotalCartValue,
  calculateEarnableCashbackPoints,
} from '../utils';
import { LIVELIKE_REWARD_ITEM_ID } from '../constants/livelike';
import axios from 'axios';
import LiveLike from '@livelike/engagementsdk';

const CartScreen = () => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;
  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;
  const [cashbackbalance, setCashbackbalance] = useState(0);
  const [appliedcashbackbalance, setAppliedcashbackbalance] = useState(0);
  const [orderdetails, setOrderdetails] = useState(null);
  const [applycashbackbalance, setApplycashbackbalance] = useState(false);
  const appliedcashbackbalanceEl = useRef(null);
  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id));
  };
  const createOrder = async () => {
    if (applycashbackbalance && appliedcashbackbalance) {
      await axios.post(`/api/use_rewards/${userInfo.user_id}`, {
        reward_item_id: LIVELIKE_REWARD_ITEM_ID,
        reward_item_amount: appliedcashbackbalance,
      });
    }
    let credit_res = await axios.post(`/api/earn_rewards/${userInfo.user_id}`, {
      reward_item_id: LIVELIKE_REWARD_ITEM_ID,
      reward_item_amount: calculateEarnableCashbackPoints(
        getTotalCartValue(cartItems) - (appliedcashbackbalance || 0)
      ),
    });
    setOrderdetails(credit_res.data);
  };
  const applyCashback = () => {
    let c_amount = appliedcashbackbalanceEl.current.value;
    if (+c_amount <= cashbackbalance) {
      setAppliedcashbackbalance(c_amount);
    }
    appliedcashbackbalanceEl.current.value = '';
  };
  useEffect(() => {
    LiveLike.getRewardItemBalances({
      rewardItemIds: [LIVELIKE_REWARD_ITEM_ID],
    }).then((res) => {
      setCashbackbalance(res.results[0].reward_item_balance);
    });
  }, [setCashbackbalance]);
  return (
    <>
      {orderdetails ? (
        <Alert variant='success'>
          <Alert.Heading>Order Created Successfully!!</Alert.Heading>
          <p className='cashback-info'>
            Cashback Points Balance updated to&nbsp;
            <strong>{orderdetails.new_balance}</strong>
          </p>
        </Alert>
      ) : (
        <></>
      )}
      <Row>
        <Col md={8}>
          <h1>Shopping Cart</h1>
          {cartItems.length === 0 ? (
            <div>
              Your cart is empty <Link to='/'>Go Back</Link>
            </div>
          ) : (
            <ListGroup variant='flush'>
              {cartItems.map((item) => (
                <ListGroup.Item key={item.product}>
                  <Row>
                    <Col md={2}>
                      <Image src={item.image} alt={item.name} fluid rounded />
                    </Col>
                    <Col md={3}>
                      <Link to={`/product/${item.product}`}>{item.name}</Link>
                    </Col>
                    <Col md={2}>Rs&nbsp;{item.price}</Col>
                    <Col md={2}>
                      <Form.Control
                        type='number'
                        value={item.qty}
                        step='any'
                        min='1'
                        disabled
                        readOnly
                      />
                    </Col>
                    <Col md={2}>
                      <Button
                        type='button'
                        variant='light'
                        onClick={() => removeFromCartHandler(item.product)}
                      >
                        <i className='fas fa-trash'></i>
                      </Button>
                    </Col>
                  </Row>
                </ListGroup.Item>
              ))}
            </ListGroup>
          )}
        </Col>
        <Col md={4}>
          <Card>
            <ListGroup variant='flush'>
              <ListGroup.Item>
                <h2>Subtotal ({getCartItemCount(cartItems)}) items</h2>
                Rs&nbsp;
                {getTotalCartValue(cartItems)}
              </ListGroup.Item>
              <ListGroup.Item>
                <Form.Check
                  type='checkbox'
                  id='apply-cashback'
                  label='Apply CashBack?'
                  checked={applycashbackbalance}
                  onChange={() => {
                    setApplycashbackbalance(!applycashbackbalance);
                    setAppliedcashbackbalance(0);
                  }}
                />
                <Form.Text>Available Balance - {cashbackbalance}</Form.Text>
              </ListGroup.Item>
              {applycashbackbalance ? (
                <ListGroup.Item>
                  <Row className='align-items-center'>
                    <Col xs='auto'>
                      <Form.Label
                        htmlFor='appliedcashbackbalance'
                        visuallyHidden
                      >
                        CashBack Balance Amount
                      </Form.Label>
                      <Form.Control
                        className='mb-2'
                        id='appliedcashbackbalance'
                        placeholder='Enter CashBack Amount'
                        ref={appliedcashbackbalanceEl}
                      />
                    </Col>
                    <Col xs='auto'>
                      <Button
                        type='submit'
                        className='mb-2'
                        onClick={applyCashback}
                      >
                        Apply
                      </Button>
                    </Col>
                  </Row>
                  {appliedcashbackbalance ? (
                    <Row className='align-items-center'>
                      <Form.Text>
                        Cashback Balance Applied - {appliedcashbackbalance}
                      </Form.Text>
                    </Row>
                  ) : (
                    <></>
                  )}
                </ListGroup.Item>
              ) : (
                <></>
              )}

              <ListGroup.Item>
                <h2>Payable Amount</h2>
                Rs&nbsp;
                {getTotalCartValue(cartItems) - (appliedcashbackbalance || 0)}
              </ListGroup.Item>
              <ListGroup.Item>
                <Button
                  type='button'
                  className='btn-block mb-3'
                  disabled={cartItems.length === 0}
                  onClick={createOrder}
                >
                  Proceed To Checkout
                </Button>
                <div className='cashback-info'>
                  You will earn&nbsp;
                  <strong>
                    {calculateEarnableCashbackPoints(
                      getTotalCartValue(cartItems) -
                        (appliedcashbackbalance || 0)
                    )}
                    &nbsp;Cashback Points
                  </strong>
                  &nbsp;on this order
                </div>
              </ListGroup.Item>
            </ListGroup>
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default CartScreen;
