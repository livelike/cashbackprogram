import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import FormContainer from '../components/FormContainer';
import { login } from '../actions/userActions';
import { useNavigate } from 'react-router-dom';

const LoginScreen = () => {
  const [phone, setPhone] = useState('');
  const [otp, setOtp] = useState('');

  const dispatch = useDispatch();
  let navigate = useNavigate();

  const userLogin = useSelector((state) => state.userLogin);
  const { loading, error, userInfo } = userLogin;

  useEffect(() => {
    if (userInfo) {
      navigate('/');
    }
  }, [navigate, userInfo]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(login(phone, otp));
  };

  return (
    <FormContainer>
      <h1>Sign In</h1>
      {loading && <></>}
      <Form onSubmit={submitHandler}>
        <Form.Group className='mb-3' controlId='phone'>
          <Form.Label>Phone Number</Form.Label>
          <Form.Control
            type='tel'
            pattern='[0-9]{10}'
            placeholder='Enter phone number'
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Form.Group className='mb-3' controlId='otp'>
          <Form.Label>OTP</Form.Label>
          <Form.Control
            type='text'
            placeholder='Enter OTP'
            value={otp}
            onChange={(e) => setOtp(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Button type='submit' className='mb-3' variant='primary'>
          Sign In
        </Button>
        <Form.Group controlId='error'>
          <Form.Text>{error}</Form.Text>
        </Form.Group>
      </Form>
    </FormContainer>
  );
};

export default LoginScreen;
