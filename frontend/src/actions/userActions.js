import axios from 'axios';
import {
  USER_LOGIN_FAIL,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
} from '../constants/userConstants';
import LiveLike from '@livelike/engagementsdk';
import { LIVELIKE_ENDPOINT, LIVELIKE_CLIENT_ID } from '../constants/livelike';

export const login = (phone, otp) => async (dispatch) => {
  try {
    dispatch({
      type: USER_LOGIN_REQUEST,
    });

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const { data } = await axios.post('/api/login', { phone, otp }, config);

    await LiveLike.init({
      clientId: LIVELIKE_CLIENT_ID,
      endpoint: LIVELIKE_ENDPOINT,
      accessToken: data.livelike_access_token,
    });

    dispatch({
      type: USER_LOGIN_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: USER_LOGIN_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
