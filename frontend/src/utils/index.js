export const calculateEarnableCashbackPoints = (orderValue) =>
  Math.floor((orderValue * 20) / 10000);

export const getTotalCartValue = (cartItems) =>
  cartItems.reduce((acc, item) => acc + item.qty * item.price, 0).toFixed(2);

export const getCartItemCount = (cartItems) =>
  cartItems.reduce((acc, item) => acc + item.qty, 0);
