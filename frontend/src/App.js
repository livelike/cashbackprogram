import { Container } from 'react-bootstrap';
import Header from './components/Header';
import Footer from './components/Footer';
import HomeScreen from './screens/HomeScreen';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ProductScreen from './screens/ProductScreen';
import CartScreen from './screens/CartScreen';
import LoginScreen from './screens/LoginScreen';
import { PrivateRoute } from './PrivateRoute';

function App() {
  return (
    <Router>
      <Header />
      <main className='py-3'>
        <Container>
          <Routes>
            <Route path='/' element={<PrivateRoute component={HomeScreen} />} />
            <Route path='/login' element={<LoginScreen />} />
            <Route
              path='/product/:id'
              element={<PrivateRoute component={ProductScreen} />}
            />
            <Route
              path='/cart'
              element={<PrivateRoute component={CartScreen} />}
            />
          </Routes>
        </Container>
      </main>
      <Footer />
    </Router>
  );
}

export default App;
