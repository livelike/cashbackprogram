import express from 'express';
import bodyParser from 'body-parser';
import products from './data/products.js';
import users from './data/users.js';
import dotenv from 'dotenv';
import { APPLICATION_URL, API_TOKEN } from './constants/livelike.js';
import axios from 'axios';
const headers = {
  'Content-Type': 'application/json',
  Authorization: 'Bearer ' + API_TOKEN,
};
dotenv.config();
const app = express();

let livelike_application = null;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('API is running...');
});

app.get('/api/products', (req, res) => {
  res.json(products);
});

app.get('/api/products/:id', (req, res) => {
  const product = products.find((p) => p._id === req.params.id);
  res.json(product);
});

app.post('/api/login', async (req, res) => {
  if (!livelike_application) {
    let livelike_application_data = await axios.get(APPLICATION_URL);
    livelike_application = livelike_application_data.data;
  }
  const user = users.find((p) => p.phone === req.body.phone);
  if (user && req.body.otp === '1234') {
    if (!user.livelike_access_token) {
      const livelike_user = await axios.post(livelike_application.profile_url, {
        nickname: user.phone,
      });
      user.livelike_access_token = livelike_user.data.access_token;
      user.livelike_profile_id = livelike_user.data.id;
    }
    res.json(user);
  } else {
    const statusCode = 401;
    res.status(statusCode);
    res.json({
      message: user ? 'wrong OTP entered' : 'Invalid Phone Number',
    });
  }
});

app.post('/api/earn_rewards/:user_id', async (req, res) => {
  try {
    if (!livelike_application) {
      let livelike_application_data = await axios.get(APPLICATION_URL);
      livelike_application = livelike_application_data.data;
    }
    const user = users.find((p) => p.user_id == req.params.user_id);
    if (user) {
      const transaction = await axios.post(
        user.livelike_reward_item_credit_url,
        req.body,
        {
          headers: headers,
        }
      );
      res.json(transaction.data);
    } else {
      const statusCode = 500;
      res.status(statusCode);
      res.json({
        message: 'Invalid user_id',
      });
    }
  } catch (err) {
    console.log(err);
    const statusCode = 500;
    res.status(statusCode);
    res.json(err);
  }
});

app.post('/api/use_rewards/:user_id', async (req, res) => {
  try {
    if (!livelike_application) {
      let livelike_application_data = await axios.get(APPLICATION_URL);
      livelike_application = livelike_application_data.data;
    }
    const user = users.find((p) => p.user_id == req.params.user_id);
    if (user) {
      const transaction = await axios.post(
        user.livelike_reward_item_debit_url,
        req.body,
        {
          headers: headers,
        }
      );
      res.json(transaction.data);
    } else {
      const statusCode = 500;
      res.status(statusCode);
      res.json({
        message: 'Invalid user_id',
      });
    }
  } catch (err) {
    console.log(err);
    const statusCode = 500;
    res.status(statusCode);
    res.json(err);
  }
});

const PORT = process.env.PORT || 5001;

app.listen(
  PORT,
  console.log(`server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
);
