const users = [
  {
    user_id: 1,
    name: 'John Doe',
    phone: '1234567890',
    livelike_access_token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjdhMGJlNTJjLWE3ZmMtNGJkOS04OGY3LWE3YzgwZmZmOGQ1MyIsImlzcyI6ImJsYXN0cnQiLCJpYXQiOjE2NDg0NTAwMjYsImNsaWVudF9pZCI6IkdhRUJjcFZyQ3hpSk9TTnU0YnZYNmtyRWFndXhIUjlIbHA2M3RLNkwiLCJhY2Nlc3NfdG9rZW4iOiIyZWEwNzllNzhhZThjZWU1NWZjMjI4MzU2ZGE3N2RmNzM1MWNiZjkxIn0.xk2iNE07Tn_gVXcSswVtdF-n1enwdtDQclKjRny6-qg',
    livelike_profile_id: '7a0be52c-a7fc-4bd9-88f7-a7c80fff8d53',
    livelike_reward_item_credit_url:
      'https://cf-blast-game-changers.livelikecdn.com/api/v1/profiles/7a0be52c-a7fc-4bd9-88f7-a7c80fff8d53/reward-item-credits/',
    livelike_reward_item_debit_url:
      'https://cf-blast-game-changers.livelikecdn.com/api/v1/profiles/7a0be52c-a7fc-4bd9-88f7-a7c80fff8d53/reward-item-debits/',
  },
  {
    user_id: 2,
    name: 'Jane Doe',
    phone: '9876543210',
    livelike_access_token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImYwOThjOTMyLTFiNDQtNDM4YS1iNmRhLTQ3ZDk2ZWQ2MmY2ZCIsImlzcyI6ImJsYXN0cnQiLCJpYXQiOjE2NDg0NDk5NDQsImNsaWVudF9pZCI6IkdhRUJjcFZyQ3hpSk9TTnU0YnZYNmtyRWFndXhIUjlIbHA2M3RLNkwiLCJhY2Nlc3NfdG9rZW4iOiIxMjM4ZjYxOWM2NzgzNzFjNTFhNmQ1MzQzNzRiMzE3NzhiZjliYTJjIn0.GSultswoum70wIrQSwODxcCj0Fa65Y37gYQJDSAHmpY',
    livelike_profile_id: 'f098c932-1b44-438a-b6da-47d96ed62f6d',
    livelike_reward_item_credit_url:
      'https://cf-blast-game-changers.livelikecdn.com/api/v1/profiles/f098c932-1b44-438a-b6da-47d96ed62f6d/reward-item-credits/',
    livelike_reward_item_debit_url:
      'https://cf-blast-game-changers.livelikecdn.com/api/v1/profiles/f098c932-1b44-438a-b6da-47d96ed62f6d/reward-item-debits/',
  },
];

export default users;
